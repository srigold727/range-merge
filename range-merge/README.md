# Range Merge


RangeMerge is a maven based java project to merge the restricted zip code ranges.

#Requirements
Sometimes items cannot be shipped to certain zip codes, and the rules for these restrictions are stored as a series of ranges of 5 digit codes. For example, if the ranges are:

[94133,94133] [94200,94299] [94600,94699]

Then the item can be shipped to zip code 94199, 94300, and 65532, but cannot be shipped to 94133, 94650, 94230, 94600, or 94299.

Any item might be restricted based on multiple sets of these ranges obtained from multiple sources.

Given a collection of 5-digit ZIP code ranges (each range includes both their upper and lower bounds), provide an algorithm that produces the minimum number of ranges required to represent the same restrictions as the input.

#Notes
- The ranges above are just examples, your implementation should work for any set of arbitrary ranges
- Ranges may be provided in arbitrary order
- Ranges may or may not overlap
- Your solution will be evaluated on the correctness and the approach taken, and adherence to coding standards and best practices

#Test Case and Expected Results
If the input = [94133,94133] [94200,94299] [94600,94699]
Then the output should be = [94133,94133] [94200,94299] [94600,94699]

If the input = [94133,94133] [94200,94299] [94226,94399] 
Then the output should be = [94133,94133] [94200,94399]


### Range Merge Features

* Used JUnit to test different service methods
* Used Apache Utils like StringUtils and CollectionsUtils
* Used Log4J for the logging framework
* This project is maintainable, extensible, can debug the code and can be tested using JUnit.



### Installation

Run as Java Application
Run as Junit 
This can be deployed to the server and connected to any Web application controller (JSF, Angular).

#Enhancements
Services can be made RESTful API end points (if required) and connected to any client.





