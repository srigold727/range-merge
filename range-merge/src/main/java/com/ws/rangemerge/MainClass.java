package com.ws.rangemerge;

import java.util.List;

import org.apache.log4j.Logger;

import com.ws.rangemerge.model.RangeItem;
import com.ws.rangemerge.service.RangeMergeService;
import com.ws.rangemerge.service.RangeMergeServiceImpl;

/**
 * MainClass - Run this as java application to see the output result for input
 * String.
 * 
 * @author Srikanth
 *
 */
public class MainClass {
	static final Logger logger = Logger.getLogger(MainClass.class);

	public static void main(String[] args) {
		RangeMergeService rangeMergeService = new RangeMergeServiceImpl();
		try {
			List<RangeItem> result = rangeMergeService.mergeRanges("[94133,94133] [94200,94299] [94600,94699]");
			for (RangeItem item : result) {
				logger.debug(item.toString());
			}
			result = rangeMergeService.mergeRanges("[94133,94133] [94200,94299] [94226,94399]");
			for (RangeItem item : result) {
				logger.debug(item.toString());
			}
		} catch (Exception e) {
			logger.error("Error in MainClass" + e.getMessage());
		}
	}
}
