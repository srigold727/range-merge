package com.ws.rangemerge.model;

import com.ws.rangemerge.constant.Constants;

/**
 * This is model for Range Item
 * 
 *
 * @author Srikanth
 */
public class RangeItem implements Comparable<RangeItem> {
	// Lower Bound of Range
	int lower;

	// Upper Bound of Range
	int upper;

	public RangeItem() {
		this.setLower(0);
		this.setUpper(0);
	}

	public RangeItem(int lower, int upper) {
		this.setLower(lower);
		this.setUpper(upper);
	}

	public void setLower(int lower) {
		this.lower = lower;
	}

	public int getLower() {
		return this.lower;
	}

	public void setUpper(int upper) {
		this.upper = upper;
	}

	public int getUpper() {
		return this.upper;
	}

	@Override
	public String toString() {
		return Constants.openBracket + this.getLower() + Constants.numberSeparator + this.getUpper()
				+ Constants.closeBracket;
	}

	/*
	 * Compares Range Items
	 */
	public int compareTo(RangeItem rangeItem) {
		if (this.getLower() == rangeItem.getLower())
			return this.getUpper() - rangeItem.getUpper();
		return this.getLower() - rangeItem.getLower();
	}
}
