package com.ws.rangemerge.service;

import java.util.List;

import com.ws.rangemerge.exception.InvalidFormatException;
import com.ws.rangemerge.exception.RangeListEmptyException;
import com.ws.rangemerge.model.RangeItem;

/**
 * This is an interface for Range Merge Service This can be converted to RESTful
 * API and exposed as end points.
 * 
 * @see RangeMergeServiceImpl
 * @see RangeItem
 * @author Srikanth
 */
public interface RangeMergeService {

	/**
	 * This is a parsing method from string to range list
	 * 
	 * @param inputRangeString string to parse
	 * @return Parsed Range List
	 * @throws InvalidFormatException thrown if there is parsing error
	 */
	public List<RangeItem> parseInput(String inputRangeString) throws InvalidFormatException;

	/**
	 * This is a range list merging method
	 * 
	 * @param inputRangeString input Range String
	 * @return Merged Range List
	 * @throws InvalidFormatException  thrown if Range is not valid
	 * @throws RangeListEmptyException thrown if Range is empty
	 */
	public List<RangeItem> mergeRanges(String inputRangeString) throws InvalidFormatException, RangeListEmptyException;
}
