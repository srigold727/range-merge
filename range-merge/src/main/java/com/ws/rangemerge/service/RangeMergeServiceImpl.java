package com.ws.rangemerge.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.ws.rangemerge.constant.Constants;
import com.ws.rangemerge.exception.InvalidFormatException;
import com.ws.rangemerge.exception.RangeListEmptyException;
import com.ws.rangemerge.model.RangeItem;

/**
 * This is class is implementation for parsing input Range and merging ranges
 *
 * @see RangeMergeService
 * @see RangeItem
 * @author Srikanth
 */

public class RangeMergeServiceImpl implements RangeMergeService {
	public RangeMergeServiceImpl() {

	}

	/**
	 * This method will take input string and parse it and generate sorted Range
	 * Item List
	 * 
	 * @param inputRange string to parse
	 * @return Parsed and Sorted Range List
	 * @throws InvalidFormatException is thrown if Range is invalid
	 */
	public List<RangeItem> parseInput(String inputRange) throws InvalidFormatException {
		List<RangeItem> outputRangelist = new ArrayList<RangeItem>();
		String[] ranges = StringUtils.split(inputRange, Constants.listSeparator);

		for (int i = 0; i < ranges.length; i++) {
			if (!StringUtils.startsWith(ranges[i], Constants.openBracket)
					|| !StringUtils.endsWith(ranges[i], Constants.closeBracket)) {
				throw new InvalidFormatException("Invalid Range Operator");
			}

			String range = StringUtils.substring(ranges[i], 1, ranges[i].length() - 1);
			String[] bounds = StringUtils.split(range, Constants.numberSeparator);

			if (bounds != null && bounds.length != 2) {
				throw new InvalidFormatException("Invalid Range Format");
			}

			try {
				int lower = Integer.valueOf(bounds[0]);
				int upper = Integer.valueOf(bounds[1]);
				outputRangelist.add(new RangeItem(lower, upper));
			} catch (Exception e) {
				throw new InvalidFormatException("Invalid Range Numer");
			}
		}
		if (CollectionUtils.isNotEmpty(outputRangelist)) {
			Collections.sort(outputRangelist);
		}

		return outputRangelist;
	}

	/**
	 * This method will take input of list of Range and merge the Range Based on
	 * business logic and gives new merged Range Items by comparing the range of previous Range.
	 * 
	 * @param list range list to merge
	 * @return Merged Range List
	 * @throws RangeListEmptyException
	 */
	private List<RangeItem> mergeRanges(List<RangeItem> inputRange) throws RangeListEmptyException {

		if (CollectionUtils.isEmpty(inputRange)) {
			throw new RangeListEmptyException();
		}
		List<RangeItem> outputRange = new ArrayList<RangeItem>();
		RangeItem range = inputRange.get(0);
		int leftBound = range.getLower(), rightBound = range.getUpper();
		for (int i = 1; i < inputRange.size(); i++) {
			range = inputRange.get(i);
			if (range.getLower() > rightBound + 1) {
				outputRange.add(new RangeItem(leftBound, rightBound));
				leftBound = range.getLower();
				rightBound = range.getUpper();
			} else if (rightBound < range.getUpper()) {
				rightBound = range.getUpper();
			}
		}
		outputRange.add(new RangeItem(leftBound, rightBound));
		return outputRange;

	}

	/**
	 * This method will parse input string and merge the Range Based on business
	 * logic and gives new merged Range Items.
	 * 
	 * @param inputRangeString input Range String
	 * @return Merged Range List
	 * @throws InvalidFormatException RangeListEmptyException
	 */
	public List<RangeItem> mergeRanges(String inputRangeString) throws InvalidFormatException, RangeListEmptyException {
		return this.mergeRanges(this.parseInput(inputRangeString));
	}

}
