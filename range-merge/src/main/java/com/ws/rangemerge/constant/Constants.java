package com.ws.rangemerge.constant;

/**
 * This is a class with constants used in application.
 *
 * @author Srikanth
 */
public class Constants {
    // Line Separator
    public static String listSeparator = " ";

    // Number Separator
    public static String numberSeparator = ",";

    // Open Bracket
    public static String openBracket = "[";

    // Close Bracket
    public static String closeBracket = "]";
}
