package com.ws.rangemerge.exception;

/**
 * This is an exception causes when invalid format input
 *
 * @see RangeListEmptyException
 * @author Srikanth
 */
public class InvalidFormatException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 4465518575736332906L;

	public InvalidFormatException(String message) {
        super("InvalidFormat: " + message);
    }
}
