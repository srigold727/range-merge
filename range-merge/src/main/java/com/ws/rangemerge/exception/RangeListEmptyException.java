package com.ws.rangemerge.exception;

/**
 * This is an exception causes when merge empty list
 *
 * @see InvalidFormatException
 * @author Srikanth
 */
public class RangeListEmptyException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = -1425616144288324152L;

	public RangeListEmptyException() {
        super("Range List cannot be empty");
    }
}
