package com.ws.rangemerge;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.Test;

import com.ws.rangemerge.model.RangeItem;
import com.ws.rangemerge.service.RangeMergeService;
import com.ws.rangemerge.service.RangeMergeServiceImpl;

/*
 * RangeMergeTest test class will test different test cases. 
 */
public class RangeMergeTest {

	static final Logger logger = Logger.getLogger(RangeMergeTest.class);

	/*
	 * testCalcMerge test case will test Range merge with given input with expected
	 * output Range
	 */
	@Test
	public void testRangeMerge() {
		RangeMergeService rangeMergeService = new RangeMergeServiceImpl();
		// Test 1
		try {
			List<RangeItem> resultRange = rangeMergeService.mergeRanges("[94133,94133] [94200,94299] [94600,94699]");
			assertEquals(resultRange.size(), 3);
			for (RangeItem item : resultRange) {
				logger.debug(item.toString());
			}
		} catch (Exception e) {
			assertTrue(false, "Exception raised");
		}
		// Test 2
		try {
			List<RangeItem> resultRange = rangeMergeService.mergeRanges("[94133,94133] [94200,94299] [94226,94399]");
			assertEquals(resultRange.size(), 2);
			for (RangeItem item : resultRange) {
				logger.debug(item.toString());
			}
		} catch (Exception e) {
			assertTrue(false, "Exception raised");
			logger.error("RangeMergeTest.testRangeMerge Error " + e.getMessage());
		}
	}

	/*
	 * testInputParse test case will test the input parser with different input
	 * formats.
	 */
	@Test
	public void testInputParse() {
		RangeMergeService rangeMergeService = new RangeMergeServiceImpl();
		// Valid input parameter
		try {
			List<RangeItem> validInputRange = rangeMergeService.parseInput("[94133,94133] [94200,94299] [94600,94699]");
			assertEquals(validInputRange.size(), 3);
		} catch (Exception e) {
			assertTrue(false, "Exception raised");
		}
		// invalid input parameter
		try {
			rangeMergeService.parseInput("[94133,94133][94200,94299] [94600,94699]");
			assertTrue(false, "Exception not raised");
		} catch (Exception e) {
			assertTrue(true);
		}
		// invalid input parameter
		try {
			rangeMergeService.parseInput("[9413394133] [94200,94299] [94600,94699]");
			assertTrue(false, "Exception not raised");
		} catch (Exception e) {
			assertTrue(true);
		}
		// invalid input parameter
		try {
			rangeMergeService.parseInput("[94133,94133] (94200,94299] [94600,94699]");
			assertTrue(false, "Exception not raised");
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	/*
	 * testEmptyList test case will test Empty Range input.
	 */
	@Test
	public void testEmptyList() {
		RangeMergeService rangeMergeService = new RangeMergeServiceImpl();
		// Valid input parameter
		try {
			rangeMergeService.mergeRanges("[94133,94133] [94200,94299] [94600,94699]");
			assertTrue(true);
		} catch (Exception e) {
			assertTrue(false, "Exception raised");
		}
		// Empty input parameter
		try {
			rangeMergeService.mergeRanges("");
			assertTrue(false, "Exception not raised");
		} catch (Exception e) {
			assertTrue(true);
		}
	}
}
